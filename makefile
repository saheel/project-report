all: report.pdf

report.pdf: report.tex tex/* bibfile.bib
	pdflatex report.tex
	bibtex report
	pdflatex report.tex
	pdflatex report.tex

clean:
	rm report.aux report.bbl report.blg report.log report.out report.pdf *~
