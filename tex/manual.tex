\section{Tool Description and Usage}
\label{Sec:details}

In this section we will look at some of the features of SZZ++, how to
use them, and how to customize and extend SZZ++. SZZ++ is written
mostly in Python with a few helper scripts written in the statistical
programming language R.

In order to use SZZ++, one needs to have Python2.7 installed with a
few essential packages such as gitpython and psycopg2.

\subsection{`main.py', the starting point}

`main.py', as the name suggests, is the main wrapper around the
essential functions of SZZ++.  Executing this script without any
options prints the Usage Help into the terminal. As seen in Figure
\ref{fig:main.py} on the following page, not providing any arguments
results in an exception which reads, ``ValueError: 7 or more arguments
expected. 0 given.''

At the top of the Usage Help is the description of these required
command line arguments: 

\begin{figure*}[pt!]
  \includegraphics[width=18cm]{images/main_py.png}
  \caption{Executing `main.py' brings up the Usage Help for SZZ++.}
  \label{fig:main.py}
\end{figure*}

\begin{itemize}
  \item Path to the output directory
  \item Name of the project
  \item Language of the project (c, cpp, java, etc.)
  \item \textsc{Github} URL of the project
  \item Interval between snapshots given in number of months
  \item Number of processing threads you want SZZ++ to create 
  \item The SZZ++ steps you want to execute.
\end{itemize}

Following this, the Usage Help lists the steps that you can execute
with SZZ++. Essentially, these steps are the functions/scripts wrapped
inside `main.py'. Some or all of the command line arguments provided
to `main.py' are used in these steps.

Though Figure 3 shows the eighteen steps that can be executed, the
crux of SZZ++ are the initial ten steps. Rest of the steps are SZZ++
extensions that were used in recent projects. One of the principles
for SZZ++ is to make the data pipelines easier to manage. Thus once
someone starts using SZZ++ in their work, their custom list of steps
might look different.

Below the list of steps in the Usage Help are a couple of examples to
show how to call `main.py'. The first example executes the initial
five steps for the project \textsc{Libgit2}. Below, we look at the
details of these individual tasks with the running example of
\textsc{Libgit2}.

\subsection{Steps in SZZ++}

Each step in SZZ++ is simply a script that gets called from `main.py'
with appropriate arguments, performs some action, and writes the
output to disk. Because of this, generally speaking, each step is
dependent on the output of the previous step.

The essential steps of SZZ++, that is, the scripts that extract bug
and type data from given projects employ parallel processing wherever
the tasks are embarrassingly parallel. As mentioned before, this
behavior is controlled by the sixth argument to `main.py'.

Some of the steps might seem to be doing rather insignificant tasks,
but this is a deliberate choice to increase modularity. We will now
discuss what some of these steps do and what to expect when you run
them:\\

\noindent \textbf{1. Clone the given project: } Quite simply,
this script check if the project exists at the provided
\textsc{Github} URL, and tries to download a copy of its latest
version.\\

% TODO INSERT IMAGE

\noindent \textbf{2. Dump the snapshots: } This step will check out different
versions of the project every N months, where N is a user-given
argument. If you only care about the first and last version, then N
should equal the entire duration of the project. You can manually
delete some versions/snapshots from the output of this step, if it
suits your purpose. The next step only considers and works on the
snapshots present in the output directory after.\\

\noindent \textbf{3. Dump the commit change history: } This step
extracts all the changes committed to the project history between each
pair of snapshots. By a commit change we mean the old and new versions
of files changed in a commit. Figure \ref{fig:ss_dump} shows this step
being performed for a few snapshots of the \textsc{Libgit2} project.\\

\begin{figure*}[pt!]
  \includegraphics[width=18cm]{images/snapshot_dump.png}
  \caption{Saving various snapshots (aka versions) of the project to
    disk. Test files are ignored.} 
  \label{fig:ss_dump}
\end{figure*}

\noindent \textbf{4. Get the list of bug-fix commits: }  Getting hold of
the list of bug-fix commits depends on the type of bugs we are dealing
with -- development-time or post-production. For development-time
bugs, the user can run the `R/scripts/bug\_predict.R' script provided
as part of SZZ++. Whereas, for post-production bugs the list can
easily be obtained from a bug database like \textsc{Bugzilla}. Since
this detail depends on the specific software project being used, we
cannot automate it as part of SZZ++. 

As of now it is assumed the user has a way of fetching the list of
bug-fix commits. Thus, this step simply fetches the list of bug-fix
commits that the user has stored in their database. \\

\noindent \textbf{5 and 6. Extract  and save bug data: } This is the quintessential
step in the list where bug data is extracted using the SZZ
algorithm. The output consists of multiple CSV files, one for each
snapshot and a single merged CSV file consisting bug data for all the
snapshots. This single CSV file is named `ss\_bugdata.csv' and resides
in the `/data/corpus/libgit2' directory in case of the project \textsc{Libgit2}.

\begin{figure}[ht!]
  \includegraphics[width=8.5cm]{images/szz_processing.png}
  \caption{SZZ in action. Collecting bug data for a few snapshots is shown.}
  \label{fig:szz_processing}
\end{figure}

For each buggy line found in the snapshots, SZZ++ records
several metadata: project\_name, file\_name, commit\_sha, line\_num, is\_new, is\_buggy,
bugfix\_snapshot, bugfix\_file\_name, bugfix\_sha, bugfix\_line\_num. The
last five columns record information on where the buggy line was
eventually fixed; rest of the columns is information about the buggy
line itself. Figure \ref{fig:szz_processing} shows the SZZ step being
run for our project \textsc{Libgit2}.

Step 6 simply stores these CSV files in a SQL database, which is more
suited for further processing. The details of the SQL database can be
edited in the `src/szz/dump\_bugdata\_into\_psql\_table.py' script. \\

\noindent \textbf{7, 8, and 9. Generate and store type data: } These
three steps generate Abstract Syntax Trees (ASTs) for the snapshots of
our interest, determine the type of each line in these snapshots, and
store all this type in CSV files and later into the SQL database.

These steps are not quite part of the SZZ algorithm but add a
considerable amount of metadata to the buggy lines as well as
non-buggy lines in the codebase.\\ 

\noindent \textbf{10. Merge type and bug data: } This steps simply
merges the generated SQL tables for type and bug data for each line
in the snapshots. Since the tables may end up containing millions of
rows, it is a fairly time consuming step. An rough estimate of
processing time is given at the beginning of the SQL merge.   

At the end of this merge, we have the following columns for each row
in the table (which denotes a line in some snapshot of the project):
project, snapshot, file\_name, sha, line\_num, line\_type, parent1,
parent2, parent3, parent4, parent5, parents\_all, ast\_depth,
loop\_nesting\_depth, is\_buggy, bf\_ss, bf\_file\_name, bf\_sha,
bf\_line\_num. Apart from the bug data columns, now we have the type
of each line, its parents in the AST, and its nesting depth.

Steps 13 through 16 optionally add containing AST block and containing
function information to this set.

\subsection{Customization and Extension of SZZ++} 

\noindent \textbf{Mapping onto single or multiple snapshots: } SZZ++ also
provides an option to map the buggy lines onto all the snapshots after
the bug was introduced and before it was fixed, OR only onto the
snapshot nearest to the bug-introducing commit. This option allows the
user more control over what kind of snapshot bug data should be
collected. To enable this option, the user can check out the `blame\_on\_only\_one\_ss'
branch of the repository.\\

\begin{figure}[h!]
  \includegraphics[width=8cm]{images/szz_step.png}
  \caption{A look inside `main.py' showing the wrapper code for Step 5.}
  \label{fig:szz_step}
\end{figure}

\noindent \textbf{Adding custom steps: } Figure \ref{fig:szz_step} shows the
code for Step 5 -- Generate bug data (SZZ) -- as it appears in
`main.py'. You can easily call your own scripts by adding such small
snippets of code to `main.py'. 

