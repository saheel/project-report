\section{Background and Methodology}
\label{Sec:szz}

\subsection{SZZ: the algorithm}

In their 2005 paper, \'{S}liwerski, Zimmermann, and Zeller
\cite{sliwerski2005changes} first discussed an algorithm to
automatically locate fix-inducing changes by linking the version
history of a project with its bug data. They worked with \textsc{CVS}
as the version control system and \textsc{Bugzilla} as the bug
tracking system. The basic idea behind their algorithm, henceforth
referred to as SZZ, is as follows:

\begin{itemize}
  \item Start with a bug report in the bug database, indicating a
    \textit{fixed problem}; 
  \item Extract the associated change from the version archive, which
    gives us the \textit{location} of the fix; 
  \item Determine the \textit{earlier change} at this location before
    the bug was reported. 
\end{itemize}

\begin{figure}[ht!]
  \includegraphics[width=8.5cm]{images/szz.png}
  \caption{The SZZ algorithm tracking the fix-inducing changes.}
  \label{fig:szz}
\end{figure}

This \textit{earlier change} is a candidate for \textit{fix-inducing
  changes}. The example in Figure \ref{fig:szz} makes it clearer. Bug
ID 42233 was fixed in version 1.18 and the associated changes are
tracked back in the version history using the \textit{annotate}
command of CVS. Amongst these changes, we can ignore the ones
committed in versions 1.14 and 1.16 since they occur after the bug was
reported. Whereas the change made in version 1.11 were committed
before the bug was reported, hence it is noted as a fix-inducing
change. 

\subsection{SZZ++: extensions and improvements over SZZ}
It is important to note that our implementation of the idea presented
in the SZZ algorithm has significant differences. Here we discuss some
of them.\\\\
\textbf{Development-time bugs: } Firstly, SZZ++ has the
capability to work with development-time bugs as well as
post-production bugs. For the former, since we do not have any bug
database during development time, we have to parse \textsc{Git} commit
messages and look for 
error-related syntactic elements in order to get hold of bug-fix
commits. Specifically, we convert each commit message to a 
bag-of-words, stem the bag-of-words using standard text processing
techniques, and then mark the commit as a bug-fix commit if it
contains at least one of these keywords: `error', `bug', `fix',
`issue', `mistake', `incorrect', `fault', `defect', `flaw', and
`type'. This strategy is based on prior work by Mockus et
al. \cite{Mockus}, and Ray et al. \cite{ray2016naturalness}
\cite{Ray:2014}. 

As far as post-production bugs are concerned, SZZ++ has an option to
include a bug database complete with time-stamps for each bug, so that
it can filter out potential bug-fix change candidates as was done in
the original SZZ algorithm. More details are given in the following section.\\\\
\textbf{Git-specific improvements: } Secondly, since we are working
with \textsc{Git} instead of CVS, we have access to much more
sophisticated tracking of each line of code of our projects. As noted
by Kim et al. \cite{kim2006automatic}, the original SZZ algorithm has
a couple of limitations:

\begin{itemize}
  \item The tracked lines do not have sufficient function/method
    annotation information which can lead to some erroneous reporting
    of bug-fix changes.
  \item The changes to comments, blank lines, and formatting are
    flagged as bug-fixes even though clearly they are only cosmetic
    changes. 
\end{itemize}

The first one is simply a limitation of CVS, the version
control system used in the original investigation as it does not care
about the function containing each line of code. Whereas the second
limitation is a result of not looking into the contents of each
changed line while tracking it. As recommended by Kim et al., we have
improved upon both these areas in our implementation. Each line
in our resultant database has detailed annotation information,
including not just containing method but also containing AST block, if
any.\\\\
\textbf{Working with snapshots: } And finally, SZZ++ allows you to
conduct longitudinal experiments. It records bug data for various
snapshots/versions throughout the history of the project. This
is done in three steps: identifying lines related to a bug,
identifying commits that introduced the bug, and mapping the buggy
lines to the snapshots of interest.

The first two steps are the same as in SZZ, albeit using
development-time bugs. Now, once we know where the buggy lines
originate, we use \textsc{git-blame}, a functionality provided by
\textsc{Git}, along with the `--reverse' option to locate these lines
in various snapshots. \textsc{git-blame} essentially maps the buggy
lines to specific snapshots.

\begin{figure}[ht!]
  \includegraphics[width=8.5cm]{images/snapshot.png}
  \caption{The SZZ++ algorithm mapping buggy lines onto multiple
    snapshots. Vertical dashed lines are snapshots, named S1...S3,
    whereas triangles marked on the Project Time Line are commits,
    named c1...c5.}
  \label{fig:ss_mapping}
\end{figure}

Figure \ref{fig:ss_mapping} taken from \cite{ray2016naturalness}
illustrates this procedure. Here, c4 is the bug-fix commit and the 
corresponding buggy lines (color-marked red in the old version of
the file) are found to originate from two earlier commits c1 and c2,
as indicated by the blue curved arrows. SZZ++ then maps the buggy
lines from c1 to S1 and S2, as indicated by the curved arrows. On
the other hand, the buggy lines from c2 can only be mapped onto S2
because they don't exist in S1.

At the end of these there steps, we know exactly which lines in each
of our snapshots are buggy (and were consequently fixed in later
snapshots) and which lines are benign. 